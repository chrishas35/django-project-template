import os
import sys
import urlparse

from {{ project_name }}.settings.base import *


# Helper lambda for gracefully degrading environmental variables:
env = lambda e, d: os.environ[e] if e in os.environ else d

DEBUG = TEMPLATE_DEBUG = False

########## DATABASE CONFIGURATION
# See: https://devcenter.heroku.com/articles/django#postgres_database_config
urlparse.uses_netloc.append('postgres')
urlparse.uses_netloc.append('mysql')

try:
    # Check to make sure DATABASES is set in settings.py file.
    # If not default to {}

    if 'DATABASES' not in locals():
        DATABASES = {}

    if 'DATABASE_URL' in os.environ:
        url = urlparse.urlparse(os.environ['DATABASE_URL'])

        # Ensure default database exists.
        DATABASES['default'] = DATABASES.get('default', {})

        # Update with environment configuration.
        DATABASES['default'].update({
            'NAME': url.path[1:],
            'USER': url.username,
            'PASSWORD': url.password,
            'HOST': url.hostname,
            'PORT': url.port,
        })
        if url.scheme == 'postgres':
            DATABASES['default']['ENGINE'] = 'django.db.backends.postgresql_psycopg2'

        if url.scheme == 'mysql':
            DATABASES['default']['ENGINE'] = 'django.db.backends.mysql'
except Exception:
    print 'Unexpected error:', sys.exc_info()
########## END DATABASE CONFIGURATION

########## S3 for file storage
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
STATICFILES_STORAGE = '{{ project_name }}.utils.S3HashedFilesStorage'

AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID', '')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY', '')
AWS_STORAGE_BUCKET_NAME = env('AWS_STORAGE_BUCKET_NAME', '')
AWS_QUERYSTRING_AUTH = False

STATIC_URL = 'https://s3.amazonaws.com/%s/' % AWS_STORAGE_BUCKET_NAME
##########

########## Sentry
INSTALLED_APPS += ('raven.contrib.django',)
##########
