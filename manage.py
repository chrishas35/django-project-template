#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # Because we have multiple settings files, this is commented out by default,
    # you should set DJANGO_SETTINGS_MODULE in the enviroment.
    # os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ project_name }}.settings")

    if not os.environ.has_key('DJANGO_SETTINGS_MODULE'):
        sys.exit('ERROR: Set DJANGO_SETTINGS_MODULE in environment')

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
